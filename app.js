var activeModules = [
{ name: 'module 1' },
{ name: 'module 2' },
{ name: 'module 11' },
{ name: 'module 3' },
{ name: 'module 10' }
];



//define regular expression (numbers)
var customModuleRe = new RegExp('\\d+');

var getCustomModuleNumber = function () {

	// extract numbers from activeModules.name into array
	var array_activeModules = Object.keys( activeModules ).map( v => parseInt(customModuleRe.exec(activeModules[v].name)) );
		
	// find highest value
	var max = Math.max.apply( Math, array_activeModules );
		
	return max;

};




